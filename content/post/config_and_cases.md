+++
title = "Config Idiosyncrasies"
+++

# Rules for naming variables in `config.toml` and accessing them in templates

1. Custom variables or table names are not allowed at base-level inside `config.toml`.
  - You can **only** use the inbuilt variables outside TOML tables.. like `title` or supported table names like `params`.
2. If you want to use custom variables for theme configuration, etc, put them inside `params` table. These variables can be named as per user's liking, in any letter case. But they have to be referenced with the **exact case** inside the templates.
  - If table `params` has `Abc` key or variable, it has to be referenced as `.Site.Params.Abc`.
  - If table `params` has `aBc` key or variable, it has to be referenced as `.Site.Params.aBc`.
3. Base-level variables in `config.toml` **can be of any case**, but they **have** to be accessed in the exact case as defined in `hugo` internally!
  - While you can have `title = "something"` in `config.toml`, it **has** to be referenced in the templates with the `.Site.` prefix, like `.Site.Title`.
  - While you can have `bAsEuRl = "http://example.org"` in `config.toml`, it **has** to be referenced in the templates with the `.Site.` prefix, like `.Site.BaseURL`. None of these will work: `.Site.baseurl`, `.Site.BASEURL`, `.Site.bAsEuRl`, ..
4. On the other hand, base-level TOML table names in `config.toml` **must be all lower case**. Again they **have** to be accessed in the exact case as defined in `hugo` internally!
  - If the table name is `params`, it **has** to be referenced in the templates with the `.Site.` prefix, like `.Site.Params`.

## Test case
{{< testcase >}}
